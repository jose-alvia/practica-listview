import 'package:tarealistview/model/character.dart';


final characters = [
  Character(
    name: 'Jose Alvia',
    age: 21,
    image: 'images/jose.png',
    jobTitle: 'Android Developer',
    stars: 4.1,
  ),
  Character(
    name: 'Yandry Alemany',
    age: 22,
    image: 'images/gerard.png',
    jobTitle: 'Android Developer',
    stars: 3.8,
  ),
  Character(
    name: 'Jorge Delgado',
    age: 18,
    image: 'images/jorge.png',
    jobTitle: 'Flutter Developer',
    stars: 4.5,
  ),
  Character(
    name: 'Silvia Salome',
    age: 25,
    image: 'images/silvias.png',
    jobTitle: 'React Native Developer',
    stars: 4.2,
  ),
  Character(
    name: 'Mònica Moragues',
    age: 24,
    image: 'images/monica.png',
    jobTitle: 'Web Developer',
    stars: 3.5,
  ),
  Character(
    name: 'Pol Pitarch',
    age: 19,
    image: 'images/pol.png',
    jobTitle: 'UI Designer',
    stars: 2.9,
  ),
  Character(
    name: 'Raquel Reixach',
    age: 35,
    image: 'images/raquel.png',
    jobTitle: 'Backend Developer',
    stars: 3.8,
  ),
  Character(
    name: 'Rebeca Roig',
    age: 31,
    image: 'images/rebeca.png',
    jobTitle: 'Project Manager',
    stars: 4.6,
  ),
  Character(
    name: 'Ricard Ricart',
    age: 22,
    image: 'images/ricard.png',
    jobTitle: 'QA Team Lead',
    stars: 4.2,
  ),
  Character(
    name: 'Sílvia Salom',
    age: 27,
    image: 'images/silvia.png',
    jobTitle: 'DevOps Team Lead',
    stars: 3.9,
  ),
];
