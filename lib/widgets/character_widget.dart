import 'package:flutter/material.dart';
import 'package:tarealistview/model/character.dart';

class CharacterWidget extends StatelessWidget {
  final Character character;

  const CharacterWidget({Key? key, required this.character}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 15.0),
      decoration: BoxDecoration(
        color: Color.fromARGB(255, 208, 203, 203),
        borderRadius: BorderRadius.all(Radius.circular(25.0)),
      ),
      child: Row(
        children: [
          Container(
            padding: const EdgeInsets.all(15.0),
            child: Image.asset(
              character.image,
              height: 120.0,
            ),
          ),
          Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  margin: const EdgeInsets.only(bottom: 10.0),
                  child: Text(
                    character.name,
                    style: TextStyle(
                      fontSize: 25.0,
                      color: Color.fromARGB(255, 5, 26, 87),
                      fontWeight:  FontWeight.bold,
                    ),
                  ),
                ),
                Row(
                  children: [
                    Container(
                      padding: EdgeInsets.all(10.0),
                      decoration: BoxDecoration(
                        color: character.stars < 4.0
                            ? Color.fromARGB(255, 210, 92, 92)
                            : Color.fromARGB(255, 190, 215, 67),
                        shape: BoxShape.circle,
                      ),
                      child: Text(
                        character.stars.toString(),
                        style: const TextStyle(
                            fontSize: 18.0, color: Colors.white),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 8.0),
                      child: Text(
                        character.jobTitle,
                        style: TextStyle(
                          color: Color.fromARGB(255, 82, 3, 254),
                          fontSize: 20.0,
                        ),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
